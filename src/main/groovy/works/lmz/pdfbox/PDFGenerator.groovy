package works.lmz.pdfbox

import org.apache.pdfbox.pdmodel.PDDocument
import works.lmz.datasource.RetailxTransaction
import works.lmz.docket.FuelUp
import works.lmz.docket.Other
import works.lmz.docket.Reciept

class PDFGenerator {

	static void pdfFromTransaction( String xmlFile ){
		RetailxTransaction source = new RetailxTransaction( PDFGenerator.class.getClassLoader().getResourceAsStream("xml/extracted/${xmlFile}.xml") )

		try {
			PDDocument document = new PDDocument()

			source.extractReceiptText().split("\\n \\n").eachWithIndex { String docket, int index ->
				if( index == 0 ){
					Reciept.render( document, source.extractStoreID(), docket )
				} else {
					if( docket.toLowerCase().contains( 'fuelup' ) ) {
						FuelUp.render( document, source.extractStoreID(), docket )
					} else {
						Other.render( document, source.extractStoreID(), docket )
					}
				}

			}

			document.save("${xmlFile}.pdf")
			document.close()
		} catch (IOException e) {
			e.printStackTrace()
		}

	}
}
