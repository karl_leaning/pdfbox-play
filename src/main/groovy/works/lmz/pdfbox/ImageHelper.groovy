package works.lmz.pdfbox

import org.apache.pdfbox.pdmodel.PDDocument
import org.apache.pdfbox.pdmodel.PDPageContentStream
import org.apache.pdfbox.pdmodel.graphics.image.LosslessFactory
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject

import javax.imageio.ImageIO
import java.awt.image.BufferedImage

class ImageHelper {

	static float insertImage( PDDocument document, PDPageContentStream contentStream, String filename ){
		BufferedImage image = ImageIO.read( ImageHelper.class.getClassLoader().getResourceAsStream( filename ) )
		PDImageXObject pdImage = LosslessFactory.createFromImage( document, image )

		float scale = image.getWidth() / ( Konstants.PAGE_WIDTH - 12 )
		float width = image.getWidth() / scale
		float height = image.getHeight() / scale

		contentStream.drawImage( pdImage, 6, Konstants.PAGE_HEIGHT - 6 - height as float, width, height )
		return height
	}
}
