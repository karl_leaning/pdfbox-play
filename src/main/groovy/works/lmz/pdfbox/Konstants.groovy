package works.lmz.pdfbox

import org.apache.pdfbox.pdmodel.common.PDRectangle

class Konstants {
	final static float POINTS_PER_INCH = 72

	/** user space units per millimeter */
	final static float POINTS_PER_MM = 1 / 25.4f * POINTS_PER_INCH as float

	final static float PAGE_WIDTH = 80.0f * POINTS_PER_MM as float
	final static float PAGE_HEIGHT = 277.0f * POINTS_PER_MM as float


	final static PDRectangle RECEIPT_80MM = new PDRectangle( PAGE_WIDTH, PAGE_HEIGHT )


	final static float TEXT_POINT_SIZE = 6
	final static float LEADING = 8.0f


}
