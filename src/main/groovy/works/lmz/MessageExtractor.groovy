package works.lmz

import works.lmz.datasource.MessageSource
import works.lmz.datasource.RetailxTransaction

class MessageExtractor {

	static void main(String[] args){

		MessageSource messageSource = new MessageSource()


		while ( messageSource.hasNext() ) {
			String raw = messageSource.getTransactionText()
			RetailxTransaction source = new RetailxTransaction( raw )

			if( source.receiptTransaction() ){
				String store = source.extractStoreID()
				String lane = source.extractTillID()
				String transaction = source.extractTransactionNumber()

				println "Store: $store Lane: $lane Transaction: $transaction"

				FileWriter out = new FileWriter( "$store-$lane-${transaction}.xml" )
				out.write( raw )
				out.close()

			}

		}






	}

}
