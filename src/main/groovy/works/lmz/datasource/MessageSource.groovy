package works.lmz.datasource

import groovy.util.slurpersupport.GPathResult

import java.util.zip.GZIPInputStream

class MessageSource {

	GPathResult xml
	Iterator iterator

	MessageSource(){
		xml = new XmlSlurper().parse( this.getClass().getClassLoader().getResourceAsStream('xml/messages.xml'))
		iterator = xml.rows.row.iterator()
	}

	String getTransactionText(){
		if( !iterator.hasNext() ){
			return null
		}
		ByteArrayInputStream bais = new ByteArrayInputStream(iterator.next().value.text().replaceAll('\\s','' ).decodeHex())
		GZIPInputStream gzip = new GZIPInputStream(bais)

		String result = gzip.text

		gzip.close()
		bais.close()

		return result
	}

	boolean hasNext(){
		return iterator.hasNext()
	}

}
