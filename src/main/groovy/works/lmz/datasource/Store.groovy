package works.lmz.datasource

class Store {

	static String[] headerText(){
		return [
			'**** New World Mount Roskill ****',
			'**** 53 MAY ROAD MOUNT ROSKILL ****',
			'** SCANES SUPERMARKET LIMITED **',
			'***** TEL: 09 621 2050 *****',
			'Online shopping is now available',
			'Download the I shop New World app',
			'Find out more at ishopnewworld.co.nz'

		].collect { String it -> it.center( 56 ) }
	}

	static String[] footerText(){
		return [
			'********** TAX INVOICE **********',
			'*** GST No. 103-700-523 ***',
			'All items GST inclusive',
			'unless otherwise specified by (*)',
			'All promotions exclude Tobacco, Gift',
			'Card, Christmas Card Purchases and',
			'Payments on Account',
			'THANK YOU FOR SHOPPING AT NEW WORLD',
		].collect { String it -> it.center( 56 ) }
	}

	static String storeBanner( String storeId ){
		if( storeId.startsWith('4') ) return 'NW'
		if( storeId.startsWith('5') ) return 'PNS'
		if( storeId.startsWith('6') ) return '4SQ'
		return 'NW'
	}
}
