package works.lmz.datasource

import groovy.util.slurpersupport.GPathResult

class XMLSource {

	GPathResult xml

	XMLSource(){
		xml = new XmlSlurper().parse( this.getClass().getClassLoader().getResourceAsStream('xml/message.xml'))
	}

	String getTransactionText(){
		return xml.'biztalk_1'.body.'ActiveStore_SalesTransaction_1.70'.TransactionText.TextData.text()
	}
}
