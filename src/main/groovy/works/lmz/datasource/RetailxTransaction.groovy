package works.lmz.datasource

import groovy.util.slurpersupport.GPathResult

class RetailxTransaction {

	GPathResult xml

	RetailxTransaction( String xmlText ){
		xml = new XmlSlurper().parseText( xmlText )
	}

	RetailxTransaction( InputStream stream ){
		xml = new XmlSlurper().parse( stream )
	}

	boolean receiptTransaction(){
		return !xml.biztalk_1.body.'ActiveStore_SalesTransaction_1.70'.TransactionText.isEmpty()
	}

	String extractReceiptText(){
		return xml.biztalk_1.body.'ActiveStore_SalesTransaction_1.70'.TransactionText.TextData.text()
	}

	String extractTransactionTotal(){
		return xml.biztalk_1.body.'ActiveStore_SalesTransaction_1.70'.TransactionText.Total.TotalAmount.text()
	}

	String extractStoreID(){
		return xml.biztalk_1.body.'ActiveStore_SalesTransaction_1.70'.StoreID.text()
	}

	String extractTillID(){
		return xml.biztalk_1.body.'ActiveStore_SalesTransaction_1.70'.TillID.text()
	}

	String extractTransactionNumber(){
		return xml.biztalk_1.body.'ActiveStore_SalesTransaction_1.70'.TransactionNumber.text()
	}

	String extractStartDateTime(){
		return xml.biztalk_1.body.'ActiveStore_SalesTransaction_1.70'.StartDateTime.text()
	}

	String extractClubCard(){
		GPathResult custInfo = xml.biztalk_1.body.'ActiveStore_SalesTransaction_1.70'.CustomerInfo
		return custInfo.isEmpty() ? "" : custInfo.ClubCard.text()
	}

}
