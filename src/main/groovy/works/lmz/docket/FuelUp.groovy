package works.lmz.docket

import org.apache.pdfbox.pdmodel.PDDocument
import org.apache.pdfbox.pdmodel.PDPage
import org.apache.pdfbox.pdmodel.PDPageContentStream
import org.apache.pdfbox.pdmodel.font.PDType1Font
import works.lmz.barcode.Barcode
import works.lmz.barcode.symbologies.Code128C
import works.lmz.datasource.Store
import works.lmz.pdfbox.ImageHelper
import works.lmz.pdfbox.Konstants

class FuelUp {

	static void render(PDDocument document, String storeId, String text ) throws IOException{
		PDPage page = new PDPage( Konstants.RECEIPT_80MM )
		document.addPage( page )
		PDPageContentStream contentStream = new PDPageContentStream(document, page)

		float imageHeight = ImageHelper.insertImage( document, contentStream, "images/fuelup/${Store.storeBanner(storeId)}.bmp" )

		contentStream.beginText()
		contentStream.setFont( PDType1Font.COURIER, Konstants.TEXT_POINT_SIZE )

		float ypos = Konstants.PAGE_HEIGHT - 6 - Konstants.LEADING - imageHeight as float
		contentStream.newLineAtOffset(7,  ypos )

		ypos -= newLine( contentStream )
		text.split("\\n")	.each { String line ->

			if( line.startsWith('BARCODE:')){
				String barcodeText = line.split(':')[1].trim()
				contentStream.endText()
				Barcode barcode = new Barcode( contentStream, new Code128C( barcodeText ), 1f, 36f )
				float xpos = ( Konstants.PAGE_WIDTH - 12 - barcode.boundingWidth ) / 2 as float
				barcode.renderAt( xpos, ypos + 6 as float )
				ypos -= 36 + Konstants.LEADING
				contentStream.beginText()
				contentStream.newLineAtOffset(7,  ypos )

			} else {
				contentStream.showText(line)
				ypos -= newLine( contentStream )
			}

		}
		contentStream.endText()
		contentStream.close()
	}

	private static float newLine( PDPageContentStream contentStream ){
		contentStream.newLineAtOffset(0, -Konstants.LEADING)
		return Konstants.LEADING
	}

}
