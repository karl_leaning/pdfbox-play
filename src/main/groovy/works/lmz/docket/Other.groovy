package works.lmz.docket

import org.apache.pdfbox.pdmodel.PDDocument
import org.apache.pdfbox.pdmodel.PDPage
import org.apache.pdfbox.pdmodel.PDPageContentStream
import org.apache.pdfbox.pdmodel.font.PDType1Font
import works.lmz.datasource.Store
import works.lmz.pdfbox.ImageHelper
import works.lmz.pdfbox.Konstants

class Other {

	static void render( PDDocument document, String storeId, String text ) throws IOException{
		PDPage page = new PDPage( Konstants.RECEIPT_80MM )
		document.addPage( page )
		PDPageContentStream contentStream = new PDPageContentStream(document, page)
		float imageHeight = ImageHelper.insertImage( document, contentStream, "images/banners/${Store.storeBanner(storeId)}.bmp" )
		contentStream.beginText()
		contentStream.setFont( PDType1Font.COURIER, Konstants.TEXT_POINT_SIZE )
		contentStream.newLineAtOffset(7,  Konstants.PAGE_HEIGHT - 6 - Konstants.LEADING - imageHeight as float)
		// header text
		Store.headerText().each { String line ->
			contentStream.showText(line)
			contentStream.newLineAtOffset(0, -Konstants.LEADING)
		}
		contentStream.newLineAtOffset(0, -Konstants.LEADING)
		// docket text
		text.split("\\n").each { String line ->
			contentStream.showText(line)
			contentStream.newLineAtOffset(0, -Konstants.LEADING)
		}
		contentStream.endText()
		contentStream.close()
	}
}
