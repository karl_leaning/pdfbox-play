package works.lmz.barcode

interface Symbology {

	String render()
}
