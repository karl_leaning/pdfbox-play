package works.lmz.barcode.symbologies

import works.lmz.barcode.Symbology
import works.lmz.barcode.exception.InvalidBarcodeException

class ITF14 implements Symbology {

	private Map<String,String> ITF14_Code = [
		'0':'NNWWN', '1':'WNNNW', '2':'NWNNW', '3':'WWNNN', '4':'NNWNW',
		'5':'WNWNN', '6':'NWWNN', '7':'NNNWW', '8':'WNNWN', '9':'NWNWN'
	]

	private char[] data

	ITF14( String data) {

		if( !data.matches(/^\d*$/)){
			throw new InvalidBarcodeException( 'Numeric characters only' )

		}

		if( data.length() < 13 || data.length() > 14 ){
			throw new InvalidBarcodeException( 'Invalid length. Length must be 13 or 14)')

		}

		this.data = checkDigit( data ).getChars()
	}

	/**
	 * Encode the raw data using the ITF-14 algorithm.
	 *
	 * @return Encoded value
	 */

	@Override
	String render() {
		//check length of input

		StringBuilder result = new StringBuilder("1010")

		(0..6).each {
			char[] bars = ITF14_Code[ data[ it * 2 ].toString() ].getChars()
			char[] spaces = ITF14_Code[ data[ it * 2 + 1 ].toString() ].getChars()

			List<String> pattern = []

			(0..4).each {
				pattern.add( bars[ it ].toString() )
				pattern.add( spaces[ it ].toString() )
			}

			int colour = 1
			pattern.each{
				result.append( "$colour" )
				if( it == 'W' ){
					result.append( "$colour" )
				}
				colour = 1 - colour
			}
		}

		//add ending bars
		result.append("1101")

		return result.toString()
	}

	private static String checkDigit( String data ) {
		//calculate and include checksum if it is necessary
		if ( data.length() == 13 ) {
			int total = 0

			int multiplier = 3
			data.getBytes().each { it ->
				total += ( it - 48 ) * multiplier
				multiplier = 4 - multiplier
			}
			return data + ( 10 - total % 10 ) % 10
		}
		return data
	}
}
