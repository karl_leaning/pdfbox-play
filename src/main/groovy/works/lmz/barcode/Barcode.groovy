package works.lmz.barcode

import org.apache.pdfbox.pdmodel.PDPageContentStream

class Barcode {

	final PDPageContentStream contentStream
	final Symbology symbology
	final float barWidth
	final float barHeight

	private String barData
	private float xpos
	private float ypos

	float boundingWidth

	Barcode(PDPageContentStream contentStream, Symbology symbology, float barWidth, float barHeight ){
		this.contentStream = contentStream
		this.symbology = symbology
		this.barWidth = barWidth
		this.barHeight = barHeight
		barData = symbology.render()
		this.boundingWidth = barData.length() * barWidth as float
	}

	Barcode setOrigin( float xpos, float ypos ){
		this.xpos = xpos
		this.ypos = ypos
		return this
	}

	void renderAt( float xpos, float ypos ){
		setOrigin( xpos, ypos )
		render()

	}

	void render( ){
		contentStream.setLineWidth( barWidth )
		barData.getChars().each {
			if( it == '1' ){
				contentStream.moveTo(xpos, ypos)
				contentStream.lineTo(xpos, ypos - barHeight as float)
				contentStream.stroke()
			}
			xpos += barWidth
		}

	}

}
