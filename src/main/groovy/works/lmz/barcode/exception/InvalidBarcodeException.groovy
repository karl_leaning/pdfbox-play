package works.lmz.barcode.exception

class InvalidBarcodeException extends Exception{

	InvalidBarcodeException( String message ){
		super( message )
	}

}
